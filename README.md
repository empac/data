# EMPAC data

Data extracted from the EMPAC corpus.

## Description

The EMPAC data has been extracted from the [EMPAC corpus](https://bitbucket.org/empac/corpus/src/master/), which is made up of subtitles from the contents distributed through the [Multimedia Centre of the European Parliament](https://multimedia.europarl.europa.eu), formerly known as EuroParlTV. This dataset has been produced with the [EMPAC toolkit](https://bitbucket.org/empac/toolkit/src/master/).

## Repository contents

```text
├── EMPAC_EN.xlsx
├── EMPAC_EN_description.xlsx
├── EMPAC_EN_text.xlsx
├── EMPAC_ES.xlsx
├── EMPAC_ES_description.xlsx
├── EMPAC_ES_text.xlsx
├── LICENSE.md
├── README.md
├── diff_files_srt_tmp_en.xlsx
├── diff_files_srt_tmp_es.xlsx
├── diff_files_srt_vrt_en.xlsx
├── diff_files_srt_vrt_es.xlsx
├── diff_files_srt_xml_en.xlsx
├── diff_files_srt_xml_es.xlsx
├── empac_log_check.xlsx
├── en_es_alignment_log.xlsx
├── en_es_alignment_log_new.xlsx
├── queries_wrong_segmentation_inter_en_category.xlsx
├── queries_wrong_segmentation_inter_en_type.xlsx
├── queries_wrong_segmentation_inter_en_year.xlsx
├── queries_wrong_segmentation_inter_es_category.xlsx
├── queries_wrong_segmentation_inter_es_type.xlsx
├── queries_wrong_segmentation_inter_es_year.xlsx
├── queries_wrong_segmentation_intra_en_category.xlsx
├── queries_wrong_segmentation_intra_en_type.xlsx
├── queries_wrong_segmentation_intra_en_year.xlsx
├── queries_wrong_segmentation_intra_es.xlsx
├── queries_wrong_segmentation_intra_es_category.xlsx
├── queries_wrong_segmentation_intra_es_type.xlsx
└── queries_wrong_segmentation_intra_es_year.xlsx
```

## EMPAC_EN.xlsx and EMPAC_ES.xlsx

`EMPAC_EN.xlsx` and `EMPAC_ES.xlsx` are a dump of the metadata encoded in the corpus in three sheets corresponding to three levels of analysis:

1. `texts`: containing information about the texts only, each row represents a text.
1. `subtitles`: containing information about the subtitles and the texts, each row represents a subtitle. 
1. `lines`: containing information about the lines, the subtitles, and the texts, each row represents a line.

### Information extracted about texts

Each row represents a text, each column a field describing the text:

- `text_id`: unique ID of the text
- `record`: the first part of the ID that identifies the document across languages
- `version`: the second part of the ID denoting the version of the document, mostly referring to the language
- `lang`: the language of the subtitle, ISO two-letter code
- `type`: type of programme/register (News, Interview, Background, Discovery, History, Others) 
- `category`: field/domain (EU affairs, Economy, Security, Society, World, Others)
- `year`: year of publication
- `month`: month of publication
- `day`: day of publication
- `date`: date and time of publication in ISO format 
- `duration`: in HH:MM:SS
- `subtitles`: number of subtitles in text
- `n_lines`: number of lines
- `one_liners`: number of subtitles made up of one line
- `two_liners`: number of subtitles made up of two lines
- `n_liners`: number of subtitles madep up of more than two lines
- `tokens`: number of tokens in text
- `video_url`: URL to the video
- `srt_url`: URL to the SRT file
- `description`: brief abstract as provided by the source

### Information extracted about subtitles

Each row represents a subtitle of the corpus, first the same columns as above with information about the text are given, then each column is a field describing a feature of the subtitle. Suffixes are used to distinguish to which level a column refers to, if the value is given for both text `_t` and subtitle `_s`.

- `subtitle_id`: unique ID of the subtitle (a hash, it has no semantics)
- `no`: number/position in the text of the subtitle, count starts at 1.
- `begin`: time code for the beginning of the subtitle
- `end`: time code for the end of the subtitle
- `duration_s`: duration of the display of the subtitle
- `chars`: number of characters in subtitle
- `cps`: characters per second
- `n_lines_s`: number of lines in subtitle
- `tokens_s`: number of tokens in subtitle
- `pause`: elapsed time in seconds between the current subtitle and the preceding one.

### Information extracted about lines

Each row represents a line of the corpus, first we found the same information as above, text and subtitle, then each column is a field describing a feature of the line. Suffixes are used to distinguish to which level a column referst to, if the value is given for text `_t`, subtitle `_s` and line `_l`.

- `line_id`: unique ID of th eline (a hash, it has no semantics)
- `no_l`: number/position of line in the subtitle, count starts at 1.
- `chars_l`: number of characters in line
- `tokens_l`: number of tokens in line

## EMPAC_EN_description.xlsx and EMPAC_ES_description.xlsx

`EMPAC_EN_description.xlsx` and `EMPAC_ES_description.xlsx` are a general description of each corpus. The description is a summary of the corpus in terms of size in texts, size in subtitles, size in tokens and duration in hours, by language, year, type, category, year and type, and, year and category.

The output is an Excel file with several tabs:

- `total`, the description for the corpus as a whole
- `year`, the description across all years
- `type`, the description across all types
- `category`, the description across all categories
- `year_type`, the description across all years and types
- `year_category`, the description across all years and categories

## EMPAC_EN_texts.xlsx and EMPAC_ES_texts.xlsx

`EMPAC_EN_texts.xlsx` and `EMPAC_ES_texts.xlsx` are a lighter version of `EMPAC_EN.xlsx` and `EMPAC_ES.xlsx` containing only the metadata at text level. 

## diff_files_srt_vrt, diff_files_srt_xml

`diff_files_srt_xml` compares the SRT files downloaded with the XML files generated. There are two tabs:

- `in_both`: the subtitle has been transformed successfully from SRT to XML.
- `missing_in_df2`: the subtitle has not been transformed from SRT to XML.

`diff_files_srt_vrt` compares the SRT files downloaded with the VRT files generated. There are two tabs:

- `in_both`: the subtitle has been transformed successfully from SRT to VRT.
- `missing_in_df2`: the subtitle has not been transformed from SRT to VRT.

## empac_log_check.xlsx

`empac_log_check.xlsx` is a summary of the number of videos for which the SRT could be downloaded. If TRUE the subtitles for that video were downloaded successfully, if FALSE it means that either some error happened or, more likely, the video had no SRT file available for that particular language version.

- `all_videos`: all videos for which we have a log, no grouping, the raw data to calculate the rest of aggregations
- `lang`: videos grouped by language
- `year`: videos grouped by language and year
- `type`: videos grouped by language and type
- `category`: videos grouped by language and category
- `year_type`: videos grouped by language, year and type
- `year_category`: videos grouped by language, year and category
- `uri`: videos grouped by an invariable portion of the URL (the same video has the same string in the URL for every language)
- `lang_uri`: videos grouped by language and the URI

## en_es_alignment_log_new.xlsx

`en_es_alignment_log_new.xlsx` is a record of all the pairs of subtitles aligned and a summary. The information is organized in three tabs:

- `alignments`: the pairs of documents that have been aligned, `v1` is the version in English and `v2` is the version in Spanish.
- `summary`: the number of videos for which we have the two versions `aligned`, the number of videos for which we only have the version in English (`v1_only`), the number of videos for which we only have the version in Spanish (`v2_only`). 
- `segments`: is the list of all segments aligned in the corpus.
	- `len_src`: the number of subtitles in the source segment
	- `len_tgt`: the number of subtitles in the target segment
	- `segment`: the correspondency of subtitle IDs for the segment
	- `src_id`: the list of subtitle IDs in the source version of the segment
	- `tgt_id`: the list of subtitle IDS in the target version of the segment

## queries_wrong_segmentation

Naming conventions for `queries_wrong_segmentation_{inter|intra}_{en|es}_{category|type|year}.xlsx`:

- inter- or intra-subtitle segmentation
- `en` English or `es` Spanish
- `category`, `type`, `year` the variables to the aggregations of bad segmentation

According to this convention `queries_wrong_segmentation_intra_en_year.xlsx` is the summary for wrong intra-subtitle segmentation for English subtitles by year.

Each of the files contains the following tabs:

- `feature_total`, the distribution of wrong segmentations by features/queries for the corpus as a whole
- `feature_subcorpus_long`, the distribution of wrong segmentations by features/queries across all subcorpora in long format.
- `feature_subcorpus_freq`, the distribution of wrong segmentations as absolute frequencies by features/queries across all subcorpora in wide format.
- `feature_subcorpus_rel_freq`, the distribution of wrong segmentations as relative frequencies (normalized) by features/queries across all subcorpora in wide format.
- `category_total`, the distribution of wrong segmentations by categories for the corpus as a whole
- `category_subcorpus_long`, the distribution of wrong segmentations by categories across all subcorpora in long format.
- `category_subcorpus_freq`, the distribution of wrong segmentations as absolute frequencies by categories across all subcorpora in wide format.
- `category_subcorpus_rel_freq`, the distribution of wrong segmentations as relative frequencies (normalized) by categories across all subcorpora in wide format.
